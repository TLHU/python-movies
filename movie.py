"""This module is for representing movies, or any text describing the module"""

class Movie(object):
    """And this is the documentation for this specific module"""
    
    def __init__(self, genre, length):
        self.genre = genre
        self.length = length
